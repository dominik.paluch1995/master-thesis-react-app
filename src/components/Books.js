import React, {Component} from 'react';
import "./Books.css"
import axios from 'axios';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import DeleteIcon from '@material-ui/icons/Delete';
import UpdateIcon from '@material-ui/icons/Update';
import {createBookMock} from "../mocks";
import Button from '@material-ui/core/Button';

class Books extends Component {
    startTime;
    endTime;
    counter = 0;
    removingAll = false;

    constructor() {
        super();
        this.state = {
            books: [],
            countCreator: 1000
        }
    }

    componentDidMount() {
        this.loadBooks();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.endTime = new Date();
        if (this.startTime && this.endTime) {
            // this.counter++;
            console.log(this.counter);
            if (this.counter === this.state.countCreator && !this.removingAll) {
                console.log(`Czas wyrenderowania ${this.state.countCreator} elementów: `,
                    this.calcTimeInMs(this.startTime, this.endTime) + 'ms');
            } else if (this.removingAll) {
                console.log(`Czas usunięcia wszystkich elementów z ekranu: `,
                    this.calcTimeInMs(this.startTime, this.endTime) + 'ms');
                this.removingAll = false;
            }
        }
    }

    loadBooks() {
        axios.get(`/books`)
            .then(data => this.setState({books: data.data}))
    }

    removeBook = bookId => {
        axios.delete(`/books/${bookId}`).then(() =>
            this.setState({books: this.state.books.filter(book => book._id !== bookId)}))
    };

    updateBook = book => {
        axios.put(`/books/${book._id}`, {...book, title: book.title + ' edytowana'})
            .then(() => this.loadBooks()
            )
    };

    createBook = () => {
        const startTime = new Date();

        axios.post(`/books`, createBookMock())
            .then((book) => this.setState({books: [...this.state.books, book.data]})
            );

        const endTime = new Date();
        console.log('Czas operacji tworzenia 1 książki wynosi: ' + this.calcTimeInMs(startTime, endTime) + 'ms');


    };

    createManyBooks = () => {
        this.initValues();
        const startTime = new Date();
        for (let i = 0; i < this.state.countCreator; i++) {
            axios.post(`/books`, createBookMock(`Tytuł ${i}`))
                .then(book => {
                    this.counter++
                    // this.setState({books: [...this.state.books, book.data]})
                })
        }
        this.loadBooks();

        const endTime = new Date();
        console.log('Czas operacji tworzenia ' + this.state.countCreator + ' książek wynosi: ' + this.calcTimeInMs(startTime, endTime) + 'ms');
    };

    removeAllBooks = () => {
        this.initValues();
        const startTime = new Date();
        this.removingAll = true;
        this.state.books.forEach(book => {
            axios.delete(`/books/${book._id}`)
        });
        this.loadBooks();
        const endTime = new Date();
        console.log('Czas operacji usuwania ' + this.state.books.length + ' książek wynosi: ' + this.calcTimeInMs(startTime, endTime) + 'ms');
    };

    editAllBooks = () => {
        this.initValues();
        this.state.books.forEach(book => {
            axios.put(`/books/${book._id}`, {...book, title: `${book.title} edytowana`})
        });
        this.loadBooks();
    };

    calcTimeInMs(startTime, stopTime) {
        return stopTime.getTime() - startTime.getTime();
    }

    initValues() {
        this.startTime = new Date();
        this.counter = 0;
        this.removingAll = false;
    }

    render() {
        const bookItems = this.state.books.map(book =>
            <ListItem key={book._id} className="item">
                <ListItemText primary={book.title}/>
                <div className="item__actions">
                    <UpdateIcon titleAccess="Edytuj" onClick={() => this.updateBook(book)} className="actions__update"
                                color="action"/>
                    <DeleteIcon titleAccess="Usuń" onClick={() => this.removeBook(book._id)}
                                className="actions__remove"/>
                </div>
            </ListItem>);

        return (
            <div className="container">
                <div className="actions">
                    <Button onClick={() => this.createBook()} className="actions__1" variant="contained"
                            color="primary">
                        Dodaj 1 książkę
                    </Button>
                    <Button onClick={() => this.createManyBooks()} className="actions__many" variant="contained"
                            color="secondary">
                        Dodaj {this.state.countCreator} książek
                    </Button>
                    {this.state.books.length ?
                        <Button onClick={() => this.editAllBooks()} className="actions__many" variant="contained"
                                color="secondary">
                            Edytuj wszystkie książki
                        </Button> : null}
                    {this.state.books.length ?
                        <Button onClick={() => this.removeAllBooks()} className="actions__many" variant="contained"
                                color="secondary">
                            Usuń wszystkie książki
                        </Button> : null}
                </div>
                <List component="nav">
                    {bookItems}
                </List>
            </div>
        )
    }
}

export default Books;
