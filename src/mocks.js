export function createBookMock(title = 'Tytuł') {
    return {
        title: title,
        price: 39,
        author: createAuthorMock(),
        category: 'Drama',
        isbn: '978-3-16-148410-0',
        publishingHouse: 'Albatros',
        releaseDate: new Date()
    }
}

export function createAuthorMock() {
    return {
        nationality: 'Polska',
        name: 'Adam',
        surname: 'Mickiewicz'
    };
}
